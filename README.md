# T.T. Burger #

Trata-se uma aplicação de leitura de banco de dados que fica hospedado no servidor da Amazon.

### O que a aplicação resolve ###

* A aplicação nos fornece em tempo real um resumo das vendas dos restaurantes, possibilitando a tomada de decisão rápida. A exemplo, lançamento de uma promoção nas midias sociais.
* É possível também verificar o histórico de vendas e metas do passado.

### O que nos agrega? ###

* No Dashboard da War Room exibirá as informações de vendas e assim permitindo implementar soluções imediatas e futuras. 
* Obter esses mesmos dados no celular em qualquer local. 

### A Aplicação ###

* Temos uma aplicação utilizando Vue.js para gerar a view de nossa pagina.
* Hoje uma das tecnologias mais usadas é React.js combinado ao poder do Express + Sequelize para esse tipo de aplicação.
* Devidos a limitações impostas não utilizamos as aplicações mais correntes do mercado.
* Para a futura utilização de Inteligência Artificial o uso de Python e flask (ou Django), é altamente recomendada principalmente pela facilidade de separar as aplicações em container.
 

### Funcionamento ###

* Temos uma single page aplication que ja nos retorna preciosos dados de venda.
    * Uma futura atualização, convêm inserir Alerts para comportamentos anomalos, como uma das lojas ficar off-line e parar de nos enviar dados.
* Abaixo temos tela dos dados de vendas de nossa API.
  
![img](https://bitbucket.org/AurelioVulcao/t.t.-burger/raw/9b1b5a2e39f44de6ba95960b76ac3b2b9cc38534/images/appImage.JPG)

### Implementações Futuras ###

* Tratar os dados exibidos e clusterizar infomações dos clientes usando inteligencia artificial.
* Permite agregar a utilização de raspagem de dados nos da o poder de análise em nossos concorrentes e combinar issa a nossa API podemos tomar decisões melhores para os nossos negócios.


### Por trás das linhas de código  ###

* Convertemos as planilhas fornecidas em uma string que utilizamos para popular o banco de dados, foi adotado código python por sua simplicidade.
~~~python
import csv

j = []
k = []
w = []
arquivo = open('venda_produtos.csv')

linhas = csv.reader(arquivo)

for linha in linhas:
    j += linha
    # print(linha)


for c in range(1, len(j)):
    k.append(j[c].split(";"))
    w.append(k[c-1][1:7])


print(w)
~~~

* Usamos código javascript para popular o banco de dados.

~~~~javascript
const db = require("./_database");

var c = []  // Dados extraidos do python
var d = []  // Dados extraidos do python
async function insertData() {
  await db.connect();

  const queryParticipante =
    "INSERT INTO vendas (data,hora,loja,id_cliente,valor_efetivo,cancelado) VALUES ($1, $2, $3, $4, $5, $6)";

  var count = 0;

  while (count <= 99) {

    count++;
  }

  await db.end();
  console.log("conexão executada");
}

async function insertData() {
  await db.connect();

  const queryParticipante =
    "INSERT INTO venda_produtos (data,hora,loja,grupo,quantidade,cancelado) VALUES ($1, $2, $3, $4, $5, $6)";

  var count = 0;

  while (count <= 724) {
    await db.query(queryParticipante, d[count]);
    console.log("venda " + count + "adicionada");
    count++;
  }

  await db.end();
  console.log("conexão executada");
}

insertData();
~~~~


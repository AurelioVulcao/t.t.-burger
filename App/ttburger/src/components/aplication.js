const metaArpoador = 11200;
const metaLeblon = 12332;
const metaBarra = 5418;

const vendaArpoador = 10200;
const vendaLeblon = 11332;
const vendaBarra = 5318;

function somaMeta() {
  soma = metaArpoador + metaLeblon + metaBarra;
  return soma;
}

function atingimentoMetaArpoador() {
  percentual = (vendaArpoador / metaArpoador) * 100;
  return Number(percentual.toFixed(2));
}

function atingimentoMetaBarra() {
  percentual = (vendaBarra / metaBarra) * 100;
  return Number(percentual.toFixed(2));
}

function atingimentoMetaLeblon() {
  percentual = (vendaLeblon / metaLeblon) * 100;
  return Number(percentual.toFixed(2));
}

function atingimentoMetaTotal() {
  percentual = (atingimentoMetaArpoador() + atingimentoMetaBarra() + atingimentoMetaLeblon())/3 ;
  return Number(percentual.toFixed(2));
}

console.log(metaArpoador);
console.log(metaLeblon);
console.log(metaBarra);
console.log(somaMeta());
console.log(atingimentoMetaTotal());
console.log(atingimentoMetaArpoador());
console.log(atingimentoMetaBarra());
console.log(atingimentoMetaLeblon());
//8,93
